//
//  SavedSubtitleUserDefaultsManager.swift
//  CustomPlayerDemo
//
//  Created by Serhiy on 1/30/18.
//  Copyright © 2018 Serhiy. All rights reserved.
//

import Foundation

class SavedSubtitleUserDefaultsManager {
    private let savedSubtitleKey = "savedSubtitleKey"
    
    public static var sharedInstance = SavedSubtitleUserDefaultsManager()
    
    private init() {
        
    }
    
    func setSavedSubtitle(subtitle: SelectSubtitleListItemViewModelType) {
        UserDefaults.standard.set(subtitle.title, forKey: savedSubtitleKey)
    }
    
    func getSavedSubtitle() -> String? {
        return UserDefaults.standard.value(forKey: savedSubtitleKey) as? String
    }
}
