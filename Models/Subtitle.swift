//
//  Subtitle.swift
//  CustomPlayerDemo
//
//  Created by Serhiy on 1/30/18.
//  Copyright © 2018 Serhiy. All rights reserved.
//

import Foundation

protocol SubtitleType {
    var title: String { get }
    var url: URL? { get }
}

class Subtitle: NSObject, SubtitleType {
    var title: String
    var url: URL?
    
    init(title: String, url: URL?) {
        self.title = title
        self.url = url
    }
    
    static func getMock() -> [SubtitleType] {
        let off = Subtitle(title: "Off", url: nil)
        let subtitle0 = Subtitle(title: "Subtitle 0", url: URL(string: "https://s3.amazonaws.com/781fkickbox/sample_subtitle.srt"))
        let subtitle1 = Subtitle(title: "Subtitle 1", url: URL(fileURLWithPath: Bundle.main.path(forResource: "Sample", ofType: "srt")!))
        
        return [off, subtitle0, subtitle1]
    }
}
