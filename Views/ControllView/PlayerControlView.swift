//
//  PlayerControlView.swift
//  CustomPlayerDemo
//
//  Created by Serhiy on 1/29/18.
//  Copyright © 2018 Serhiy. All rights reserved.
//

import Foundation
import SnapKit
import UIKit
import MBProgressHUD

protocol PlayerControlViewDelegate: NSObjectProtocol {
    func playButtonAction(_ playerControll: PlayerControlView)
}

class PlayerControlView: UIView {
    weak var delegate: PlayerControlViewDelegate!
    
    let playButton = UIButton(type: .system)
    
    convenience init () {
        self.init(frame:CGRect.zero)
    }
    
    override init (frame : CGRect) {
        super.init(frame : frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        self.backgroundColor = .clear
        
        playButton.setImage(UIImage(named: "Play_button"), for: UIControlState())
        playButton.backgroundColor = .clear
        playButton.tintColor = .white
        
        playButton.addTarget(self, action: #selector(playButtonAction), for: .touchUpInside)
        
        self.addSubview(playButton)
        playButton.snp.makeConstraints { (make) in
            make.center.equalTo(self)
            make.width.equalTo(60)
            make.height.equalTo(60)
        }
    }
    
    func setButtonToPlay() {
        playButton.setImage(UIImage(named: "Play_button"), for: UIControlState())
    }
    
    func setButtonToPause() {
        playButton.setImage(UIImage(named: "pause"), for: UIControlState())
    }
    
    func setStaticState() {
        MBProgressHUD.hide(for: self, animated: true)
        self.playButton.isHidden = false
    }
    
    func setLoadingState() {
        MBProgressHUD.showAdded(to: self, animated: true).isUserInteractionEnabled = false
        self.playButton.isHidden = true
    }
    
    @objc func playButtonAction(_ sender: UIButton) {
        delegate?.playButtonAction(self)
    }
}
