//
//  SubtitleCellTableViewCell.swift
//  CustomPlayerDemo
//
//  Created by Serhiy on 1/30/18.
//  Copyright © 2018 Serhiy. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SubtitleTableViewCell: UITableViewCell {

    @IBOutlet weak var subtitleTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension SubtitleTableViewCell {
    func configure(with model: SelectSubtitleListItemViewModelType) {
        self.subtitleTitleLabel.text = model.title
        
        if model.isChecked {
            self.accessoryView = UIImageView(image: UIImage(named: "checked"))
        } else {
            self.accessoryView = UIView()
        }
    }
}
