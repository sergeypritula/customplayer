//
//  PlayerBottomView.swift
//  CustomPlayerDemo
//
//  Created by Serhiy on 1/29/18.
//  Copyright © 2018 Serhiy. All rights reserved.
//

import UIKit

protocol PlayerBottomViewDelegate: NSObjectProtocol {
    func subtitlesAction(_ playerControll: PlayerBottomView)
    
    func startedChangingProgress(_ playerControll: PlayerBottomView)
    func changingProgress(_ playerControll: PlayerBottomView, progress: Float)
    func completedChangingProgress(_ playerControll: PlayerBottomView)
}

class PlayerBottomView: UIView {
    weak var delegate: PlayerBottomViewDelegate!
    
    var timePast = UILabel()
    var timeLeft = UILabel()
    var sliderProgress = SliderBuffering()
    var subtitlesListButton = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        subtitlesListButton.addTarget(self, action: #selector(subtitlesAction), for: .touchUpInside)
        sliderProgress.addTarget(self, action: #selector(changingProgress), for: UIControlEvents.valueChanged)
        sliderProgress.addTarget(self, action: #selector(startedChangingProgress), for: UIControlEvents.touchDown)
        sliderProgress.addTarget(self, action: #selector(completedChangingProgress), for: [UIControlEvents.touchUpInside, UIControlEvents.touchUpOutside])
        
        subtitlesListButton.setImage(UIImage(named: "subtitles"), for: UIControlState())
        self.addSubview(subtitlesListButton)
        subtitlesListButton.snp.makeConstraints { (make) -> Void in
            make.right.equalTo(self.snp.right).offset(-10)
            make.centerY.equalTo(self.snp.centerY)
            make.width.equalTo(25)
            make.height.equalTo(30)
        }
       
        timePast.text = "00:00:00"
        timePast.textColor = .white
        timePast.textAlignment = .right
        
        self.addSubview(timePast)
        timePast.snp.makeConstraints { (make) in
            make.centerY.equalTo(self)
            make.leading.equalTo(self).offset(5)
            make.width.equalTo(self).multipliedBy(0.2)
        }
        
        sliderProgress.minimumValue = 0
        sliderProgress.maximumValue = 1
        sliderProgress.setThumbImage(UIImage(named: "Oval"), for: UIControlState())
        
        self.addSubview(sliderProgress)
        sliderProgress.snp.makeConstraints { (make) in
            make.centerY.equalTo(timePast.snp.centerY)
            make.height.equalTo(timePast.snp.height).multipliedBy(0.85)
            make.width.equalTo(self).multipliedBy(0.5)
            make.left.equalTo(timePast.snp.right).offset(5)
        }
        
        timeLeft.text = "00:00:00"
        timeLeft.textColor = UIColor.white
        timeLeft.textAlignment = .left
        
        self.addSubview(timeLeft)
        timeLeft.snp.makeConstraints { (make) in
            make.centerY.equalTo(self)
            make.left.equalTo(sliderProgress.snp.right).offset(0)
            make.width.equalTo(timePast)
            make.right.lessThanOrEqualTo(subtitlesListButton.snp.left).offset(5)
        }
        
    }
    
    @objc func subtitlesAction(_ sender: UIButton) {
        delegate?.subtitlesAction(self)
    }
    
    @objc func changingProgress() {
        delegate?.changingProgress(self, progress: self.sliderProgress.value)
    }
    
    @objc func startedChangingProgress() {
        delegate?.startedChangingProgress(self)
    }
    
    @objc func completedChangingProgress() {
        delegate?.completedChangingProgress(self)
    }
    
    func updateCurrentInfo(_ currentTime:TimeInterval, maximumDuration:TimeInterval)  {
        timePast.text = String.stringFromTimeInterval(currentTime)
        timeLeft.text = String.stringFromTimeInterval(maximumDuration)
        updateSlider(currentTime, maximumDuration: maximumDuration)
    }
    
    fileprivate func updateSlider(_ currentTime:TimeInterval, maximumDuration:TimeInterval) {
        sliderProgress.setValue(Float(currentTime / maximumDuration), animated: true)
    }
    
    func updateBufferingInfo(_ bufferingTime:TimeInterval, maximumDuration:TimeInterval) {
        let buffer = Float(bufferingTime) / Float(maximumDuration)
        if !buffer.isNaN {
            sliderProgress.bufferProgress.setProgress(buffer, animated: true)
        } else {
            sliderProgress.bufferProgress.setProgress(0, animated: true)
        }
    }
    
    func resetControlInfo() {
        self.updateCurrentInfo(0, maximumDuration: 0)
        self.updateBufferingInfo(0, maximumDuration: 0)
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
