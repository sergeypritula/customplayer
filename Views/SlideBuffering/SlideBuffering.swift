//
//  SlideBuffering.swift
//  CustomPlayerDemo
//
//  Created by Serhiy on 1/29/18.
//  Copyright © 2018 Serhiy. All rights reserved.
//

import Foundation
import SnapKit

class SliderBuffering: UISlider {
    let bufferProgress =  UIProgressView(progressViewStyle: .default)
    
    convenience init () {
        self.init(frame:CGRect.zero)
    }
    
    override init (frame : CGRect) {
        super.init(frame : frame)
        setup()
    }
        
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        self.tintColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        self.minimumTrackTintColor = UIColor.clear
        self.maximumTrackTintColor = UIColor.clear
        self.setMaximumTrackImage(UIImage(), for: UIControlState())
        self.setMinimumTrackImage(UIImage(), for: UIControlState())
        self.setThumbImage(UIImage(), for: UIControlState())
        
        bufferProgress.backgroundColor = UIColor.clear
        bufferProgress.isUserInteractionEnabled = false
        bufferProgress.progress = 0.0
        bufferProgress.progressTintColor = UIColor.gray
        bufferProgress.trackTintColor = UIColor.black.withAlphaComponent(0.5)
        
        self.addSubview(bufferProgress)
        
        bufferProgress.snp.makeConstraints { (make) in
            make.top.equalTo(self)
            make.leading.equalTo(self)
            make.trailing.equalTo(self)
            make.bottom.equalTo(self)
        }
    }
}
