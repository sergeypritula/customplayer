//
//  PlayerTopView.swift
//  CustomPlayerDemo
//
//  Created by Serhiy on 1/29/18.
//  Copyright © 2018 Serhiy. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

protocol PlayerTopViewDelegate: NSObjectProtocol {
    func backButtonAction(_ playerControll: PlayerTopView)
}

class PlayerTopView: UIView {
    weak var delegate: PlayerTopViewDelegate!
    
    var backButton = UIButton(type: .system)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        backButton.setImage(UIImage(named: "Back_icon"), for: UIControlState())
        backButton.tintColor = UIColor.blue
        backButton.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5)
        
        backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        
        self.addSubview(backButton)
        backButton.snp.makeConstraints { (make) -> Void in
            make.centerX.equalTo(self.snp.centerX).multipliedBy(0.085)
            make.centerY.equalTo(self.snp.centerY).multipliedBy(1.1)
            make.width.equalTo(30)
            make.height.equalTo(30)
        }
    }
    
    @objc func backButtonAction(_ sender:UIButton) {
        delegate?.backButtonAction(self)
    }
    
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}
