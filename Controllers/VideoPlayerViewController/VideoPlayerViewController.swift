//
//  ViewController.swift
//  CustomPlayerDemo
//
//  Created by Serhiy on 1/29/18.
//  Copyright © 2018 Serhiy. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Foundation
import AVFoundation
import CoreMedia
import MBProgressHUD

let storyBoard = UIStoryboard(name: "Main", bundle: nil)

class VideoPlayerViewController: UIViewController {
    
    var videoUrl = URL(string: "http://cdn-fms.rbs.com.br/vod/hls_sample1_manifest.m3u8")
    fileprivate var player: PlayerViewController!
    fileprivate var subtitlesManager: SubtitlesHelper!
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var subtitlesLabel: UILabel!
    @IBOutlet weak var playerControllView: PlayerControlView!
    @IBOutlet weak var playerTopView: PlayerTopView!
    @IBOutlet weak var playerBottomView: PlayerBottomView!
    @IBOutlet weak var bottomViewConstraint: NSLayoutConstraint!
    
    convenience init(videoUrl: URL?) {
        self.init()
        self.videoUrl = videoUrl
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black
        
        playerBottomView.delegate = self
        playerControllView.delegate = self
        playerTopView.delegate = self
        
        self.player = PlayerViewController()
        self.player.delegate = self

        self.addChildViewController(self.player)
        self.view.insertSubview(self.player.view, belowSubview: self.contentView)
        self.player.didMove(toParentViewController: self)
        self.player.view.snp.makeConstraints { (make) in
            make.top.equalTo(self.contentView)
            make.leading.equalTo(self.contentView)
            make.trailing.equalTo(self.contentView)
            make.bottom.equalTo(self.contentView)
        }

        if let videoUrl = self.videoUrl {
            self.player.setUrl(videoUrl)
        }
        
        self.player.playbackLoops = false
        self.subtitlesManager = SubtitlesHelper(label: self.subtitlesLabel, player: self.player.player)
        
        if let title = SavedSubtitleUserDefaultsManager.sharedInstance.getSavedSubtitle() {
            if let subtitle = Subtitle.getMock().first(where: {$0.title == title}), let url = subtitle.url {
                self.subtitlesManager.open(file: url)
            }
        }
        
        let tapGestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(handleTapGestureRecognizer))
        tapGestureRecognizer.numberOfTapsRequired = 1
        self.playerControllView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func handleTap() {
        if self.playerControllView.playButton.alpha == 1 {
            UIView.animate(withDuration: 0.3, animations: {
                self.playerControllView.playButton.alpha = 0
                self.playerTopView.alpha = 0
                self.playerBottomView.alpha = 0
                self.bottomViewConstraint.constant = -(self.playerBottomView.frame.height)
            })
        } else {
            UIView.animate(withDuration: 0.3, animations: {
                self.playerControllView.playButton.alpha = 1
                self.playerTopView.alpha = 1
                self.playerBottomView.alpha = 1
                self.bottomViewConstraint.constant = 0
            })
        }
    }
    
    @objc func handleTapGestureRecognizer(_ gestureRecognizer: UITapGestureRecognizer) {
        self.handleTap()
    }
}

extension VideoPlayerViewController: PlayerTopViewDelegate {
    func backButtonAction(_ playerControll: PlayerTopView) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension VideoPlayerViewController: PlayerControlViewDelegate {
    func playButtonAction(_ playerControll: PlayerControlView) {
        switch (self.player.playbackState.rawValue) {
        case PlaybackState.stopped.rawValue:
            self.player.playFromCurrentTime()
            
        case PlaybackState.paused.rawValue:
            self.player.playFromCurrentTime()
            
        case PlaybackState.playing.rawValue:
            self.player.pause()
            
        case PlaybackState.failed.rawValue:
            if let videoUrl = self.videoUrl {
                self.player.setUrl(videoUrl)
                self.player.playFromCurrentTime()
            }
            
        default:
            self.player.pause()
        }
    }
    
}

extension VideoPlayerViewController: SelectSubtitleViewControllerDelegate {
    func performSelection(subtitle: SelectSubtitleListItemViewModelType) {
        SavedSubtitleUserDefaultsManager.sharedInstance.setSavedSubtitle(subtitle: subtitle)
        
        DispatchQueue.main.async {
            if let url = subtitle.url {
                self.subtitlesManager.open(file: url)
            } else {
                self.subtitlesManager.removeSubtitles()
            }
        }
    }
}

extension VideoPlayerViewController: PlayerBottomViewDelegate {
    func subtitlesAction(_ playerControll: PlayerBottomView) {
        guard let controller = storyBoard.instantiateViewController(withIdentifier: "SelectSubtitleViewController") as? SelectSubtitleViewController else {
            return
        }
        
        controller.viewModel.initData(with: Subtitle.getMock())
        controller.delegate = self
        controller.modalPresentationStyle = .overCurrentContext
        present(controller, animated: true, completion: nil)
    }
    
    func changingProgress(_ playerControll: PlayerBottomView, progress:Float) {
        let time = Float(self.player.maximumDuration) * progress
        self.player.seekToTime(CMTime(seconds: Double(time), preferredTimescale: self.player.durationTimescale))
    }
    
    func completedChangingProgress(_ playerControll: PlayerBottomView) {
        print("completedChangingProgress")
    }
    
    func startedChangingProgress(_ playerControll: PlayerBottomView) {
        print("startedChangingProgress")
    }
}

extension VideoPlayerViewController: PlayerViewControllerDelegate {
    func playerIdleTimerAfterPlay(_ player: PlayerViewController) {
        self.handleTap()
    }
    
    func playerReady(_ player: PlayerViewController) {
        player.playFromCurrentTime()
    }
    
    func playerCurrentTimeDidChange(_ player: PlayerViewController) {
        playerBottomView.updateCurrentInfo(player.currentTime, maximumDuration: player.maximumDuration)
    }
    
    func playerLoadedTimeDidChange(_ player: PlayerViewController) {
        playerBottomView.updateBufferingInfo(player.availableDuration(), maximumDuration: player.maximumDuration)
    }
    
    func playerPlaybackWillStartFromBeginning(_ player: PlayerViewController) {
        print("playerPlaybackWillStartFromBeginning")
    }
    
    func playerPlaybackDidEnd(_ player: PlayerViewController) {
        player.seekToTime(CMTime(seconds: 0, preferredTimescale: self.player.durationTimescale))
        playerBottomView.resetControlInfo()
    }
    
    func playerPlaybackStateDidChange(_ player: PlayerViewController) {
        print("PlaybackState: \(player.playbackState.description)")
        switch (player.playbackState.rawValue) {
        case PlaybackState.playing.rawValue:
            playerControllView.setButtonToPause()
            playerControllView.setStaticState()
        case PlaybackState.paused.rawValue:
            playerControllView.setButtonToPlay()
            
        case PlaybackState.stopped.rawValue:
            playerControllView.setButtonToPlay()
            
        case PlaybackState.failed.rawValue:
            playerControllView.setButtonToPlay()
            playerControllView.setStaticState()
        default:
            print("Unknown playbackState")
        }
    }
    
    func playerBufferingStateDidChange(_ player: PlayerViewController) {
        print("BufferingState: \(player.bufferingState.description)")
        switch (player.bufferingState.rawValue) {
        case BufferingState.unknown.rawValue:
            playerControllView.setLoadingState()
            
        case BufferingState.delayed.rawValue:
            playerControllView.setLoadingState()
            
        case BufferingState.ready.rawValue:
            playerControllView.setStaticState()
            
        default:
            print("Unknown bufferingState")
        }
    }
}
