//
//  PlayerViewController.swift
//  ShakespeareWorld
//
//  Created by Vitaliy Savchenko on 30.03.16.
//  Copyright © 2016 Onix. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation
import CoreGraphics

public enum TypeLayerVideoGravity: String {
    case ResizeAspect
    case ResizeAspectFill
    case Resize
    
    public var description: String {
        get {
            switch self {
            case .ResizeAspect:
                return AVLayerVideoGravity.resizeAspect.rawValue
            case .ResizeAspectFill:
                return AVLayerVideoGravity.resizeAspectFill.rawValue
            case .Resize:
                return AVLayerVideoGravity.resize.rawValue
            }
        }
    }
}

public enum PlaybackState: Int, CustomStringConvertible {
    case stopped = 0
    case playing
    case paused
    case failed
    
    public var description: String {
        get {
            switch self {
            case .stopped:
                return "Stopped"
            case .playing:
                return "Playing"
            case .failed:
                return "Failed"
            case .paused:
                return "Paused"
            }
        }
    }
}

public enum BufferingState: Int, CustomStringConvertible {
    case unknown = 0
    case ready
    case delayed
    
    public var description: String {
        get {
            switch self {
            case .unknown:
                return "Unknown"
            case .ready:
                return "Ready"
            case .delayed:
                return "Delayed"
            }
        }
    }
}

public protocol PlayerViewControllerDelegate: class {
    func playerReady(_ player: PlayerViewController)
    func playerPlaybackStateDidChange(_ player: PlayerViewController)
    func playerBufferingStateDidChange(_ player: PlayerViewController)
    func playerLoadedTimeDidChange(_ player: PlayerViewController)
    
    func playerPlaybackWillStartFromBeginning(_ player: PlayerViewController)
    func playerPlaybackDidEnd(_ player: PlayerViewController)
    
    func playerCurrentTimeDidChange(_ player: PlayerViewController)
    func playerIdleTimerAfterPlay(_ player: PlayerViewController)
}

//MARK: make delegate methods optional
extension PlayerViewControllerDelegate {
    func playerReady(_ player: PlayerViewController) {
        
    }
    func playerPlaybackStateDidChange(_ player: PlayerViewController) {
        
    }
    func playerBufferingStateDidChange(_ player: PlayerViewController) {
        
    }
    func playerLoadedTimeDidChange(_ player: PlayerViewController) {
        
    }
    func playerPlaybackWillStartFromBeginning(_ player: PlayerViewController) {
        
    }
    func playerPlaybackDidEnd(_ player: PlayerViewController) {
        
    }
    func playerCurrentTimeDidChange(_ player: PlayerViewController) {
        
    }
    func playerIdleTimerAfterPlay(_ player: PlayerViewController) {
        
    }
}

// KVO contexts

private var PlayerObserverContext = 0
private var PlayerItemObserverContext = 0
private var PlayerLayerObserverContext = 0

// KVO player keys

private let PlayerTracksKey = "tracks"
private let PlayerPlayableKey = "playable"
private let PlayerDurationKey = "duration"
private let PlayerRateKey = "rate"

// KVO player item keys

private let PlayerStatusKey = "status"
private let PlayerEmptyBufferKey = "playbackBufferEmpty"
private let PlayerKeepUpKey = "playbackLikelyToKeepUp"
private let PlayerLoadedTimeRangesKey = "loadedTimeRanges"

// KVO player layer keys

private let PlayerReadyForDisplay = "readyForDisplay"

// MARK: - Player

open class PlayerViewController: UIViewController {
    open var delayIdleTimer:TimeInterval = 2.0
    
    open weak var delegate: PlayerViewControllerDelegate!
    
    open func setUrl(_ url: URL) {
        // Make sure everything is reset beforehand
        if(self.playbackState == .playing){
            self.pause()
            stopIdleCountdown()
        }
        
        self.setupPlayerItem(nil)
        let asset = AVURLAsset(url: url, options: .none)
        self.setupAsset(asset)
    }
    
    open var muted: Bool! {
        get {
            return self.player.isMuted
        }
        set {
            self.player.isMuted = newValue
        }
    }
    
    open var fillMode: String! {
        get {
            return self.playerView.fillMode
        }
        set {
            self.playerView.fillMode = newValue
        }
    }
    
    open var playbackLoops: Bool {
        get {
            return (self.player.actionAtItemEnd == .none) as Bool
        }
        set {
            self.player.actionAtItemEnd = newValue ? .none : .pause
        }
    }
    
    open var playbackFreezesAtEnd: Bool!
    open var playbackState: PlaybackState!
    open var bufferingState: BufferingState!
    open var bufferSize: Double = 10
    
    open var maximumDuration: TimeInterval! {
        get {
            if let playerItem = self.playerItem {
                return CMTimeGetSeconds(playerItem.duration)
            } else {
                return CMTimeGetSeconds(kCMTimeIndefinite)
            }
        }
    }
    
    open var currentTime: TimeInterval! {
        get {
            if let playerItem = self.playerItem {
                return CMTimeGetSeconds(playerItem.currentTime())
            } else {
                return CMTimeGetSeconds(kCMTimeIndefinite)
            }
        }
    }
    
    open var naturalSize: CGSize! {
        get {
            if let playerItem = self.playerItem {
                let track = playerItem.asset.tracks(withMediaType: AVMediaType.video)[0]
                return track.naturalSize
            } else {
                return CGSize.zero
            }
        }
    }
    
    open var loadedTimeRanges: [NSValue]! {
        get {
            if let playerItem = self.playerItem {
                let timeRanges = playerItem.loadedTimeRanges
                return timeRanges
            } else {
                return []
            }
        }
    }
    
    open var currentPlayerItem: AVPlayerItem? {
        get {
            if let playerItem = self.playerItem {
                return playerItem
            } else {
                return nil
            }
        }
    }
    
    open var durationTimescale: CMTimeScale {
        get {
            if let playerItem = self.playerItem {
                return playerItem.asset.duration.timescale
            } else {
                return Int32.max
            }
        }
    }
    
    fileprivate var asset: AVAsset!
    fileprivate var playerItem: AVPlayerItem?
    
    var player: AVPlayer!
    fileprivate var playerView: PlayerView!
    fileprivate var timeObserverToken: AnyObject?
    fileprivate var idleTimer:Timer!
    
    // MARK: object lifecycle
    public convenience init() {
        self.init(nibName: nil, bundle: nil)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.commonInit()
    }
    
    fileprivate func commonInit() {
        self.player = AVPlayer()
        self.player.actionAtItemEnd = .pause
        self.player.addObserver(self, forKeyPath: PlayerRateKey, options: ([NSKeyValueObservingOptions.new, NSKeyValueObservingOptions.old]) , context: &PlayerObserverContext)
        
        let interval = CMTimeMake(1, 1)
        if timeObserverToken == nil {
            timeObserverToken = player.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main) {
                [weak self] time in
                self?.delegate?.playerCurrentTimeDidChange(self!)
                } as AnyObject?
        }
        
        self.playbackLoops = false
        self.playbackFreezesAtEnd = false
        self.playbackState = .stopped
        self.bufferingState = .unknown
    }
    
    deinit {
        print("PlayerViewController DEINIT")
        self.playerView?.player = nil
        self.delegate = nil
        
        NotificationCenter.default.removeObserver(self)
        
        self.playerView?.layer.removeObserver(self, forKeyPath: PlayerReadyForDisplay, context: &PlayerLayerObserverContext)
        
        self.player.removeObserver(self, forKeyPath: PlayerRateKey, context: &PlayerObserverContext)
        
        if let timeObserverToken = timeObserverToken {
            player.removeTimeObserver(timeObserverToken)
            self.timeObserverToken = nil
        }
        
        self.player.pause()
        stopIdleCountdown()
        self.setupPlayerItem(nil)
    }
    
    // MARK: view lifecycle
    
    open override func loadView() {
        self.playerView = PlayerView(frame: CGRect.zero)
        self.playerView.fillMode = AVLayerVideoGravity.resizeAspect.rawValue
        self.playerView.playerLayer.isHidden = true
        self.view = self.playerView
        self.playerView.layer.addObserver(self, forKeyPath: PlayerReadyForDisplay, options: ([NSKeyValueObservingOptions.new, NSKeyValueObservingOptions.old]), context: &PlayerLayerObserverContext)
        
        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillResignActive), name: NSNotification.Name.UIApplicationWillResignActive, object: UIApplication.shared)
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidEnterBackground), name: NSNotification.Name.UIApplicationDidEnterBackground, object: UIApplication.shared)
    }
    
    open override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if self.playbackState == .playing {
            self.pause()
            stopIdleCountdown()
        }
    }
    
    // MARK: methods
    open func availableDuration() -> TimeInterval {
        if let item = playerItem , item.loadedTimeRanges.count > 0 {
            let timeRange: CMTimeRange = item.loadedTimeRanges.first!.timeRangeValue
            let startSeconds: Float64 = CMTimeGetSeconds(timeRange.start)
            let durationSeconds: Float64 = CMTimeGetSeconds(timeRange.duration)
            let result: TimeInterval = startSeconds + durationSeconds
            return result
        }
        return 0
    }
    
    open func playFromBeginning() {
        self.delegate?.playerPlaybackWillStartFromBeginning(self)
        self.player.seek(to: kCMTimeZero, completionHandler: { completed in
            if completed {
                self.startIdleCountdown()
            }
        })
        self.playFromCurrentTime()
    }
    
    open func playFromCurrentTime() {
        self.playbackState = .playing
        self.delegate?.playerPlaybackStateDidChange(self)
        self.player.play()
        startIdleCountdown()
    }
    
    open func pause() {
        if self.playbackState != .playing {
            return
        }
        
        self.player.pause()
        stopIdleCountdown()
        self.playbackState = .paused
        self.delegate?.playerPlaybackStateDidChange(self)
    }
    
    open func stop() {
        if self.playbackState == .stopped {
            return
        }
        
        self.player.pause()
        stopIdleCountdown()
        self.playbackState = .stopped
        self.delegate?.playerPlaybackStateDidChange(self)
        self.delegate?.playerPlaybackDidEnd(self)
    }
    
    open func seekToTime(_ time: CMTime) {
        if let playerItem = self.playerItem {
            return playerItem.seek(to: time, completionHandler: { completed in
                if completed {
                    self.startIdleCountdown()
                }
            })
        }
    }
    
    open func changeAVPlayerLayerGravity(_ type:TypeLayerVideoGravity) {
        playerView.playerLayer.videoGravity = AVLayerVideoGravity.init(rawValue: type.description)
    }
    
    // MARK: private setup
    
    fileprivate func setupAsset(_ asset: AVAsset) {
        if self.playbackState == .playing {
            self.pause()
            stopIdleCountdown()
        }
        
        self.bufferingState = .unknown
        self.delegate?.playerBufferingStateDidChange(self)
        
        self.asset = asset
        if let _ = self.asset {
            self.setupPlayerItem(nil)
        }
        
        let keys: [String] = [PlayerTracksKey, PlayerPlayableKey, PlayerDurationKey]
        
        self.asset.loadValuesAsynchronously(forKeys: keys, completionHandler: { () -> Void in
            DispatchQueue.main.sync(execute: { () -> Void in
                
                for key in keys {
                    var error: NSError?
                    let status = self.asset.statusOfValue(forKey: key, error:&error)
                    if status == .failed {
                        self.playbackState = .failed
                        self.delegate?.playerPlaybackStateDidChange(self)
                        return
                    }
                }
                
                if self.asset.isPlayable == false {
                    self.playbackState = .failed
                    self.delegate?.playerPlaybackStateDidChange(self)
                    return
                }
                
                let playerItem: AVPlayerItem = AVPlayerItem(asset:self.asset)
                self.setupPlayerItem(playerItem)
                
            })
        })
    }
    
    fileprivate func setupPlayerItem(_ playerItem: AVPlayerItem?) {
        if self.playerItem != nil {
            self.playerItem?.removeObserver(self, forKeyPath: PlayerEmptyBufferKey, context: &PlayerItemObserverContext)
            self.playerItem?.removeObserver(self, forKeyPath: PlayerKeepUpKey, context: &PlayerItemObserverContext)
            self.playerItem?.removeObserver(self, forKeyPath: PlayerStatusKey, context: &PlayerItemObserverContext)
            self.playerItem?.removeObserver(self, forKeyPath: PlayerLoadedTimeRangesKey, context: &PlayerItemObserverContext)
            
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.playerItem)
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemFailedToPlayToEndTime, object: self.playerItem)
        }
        
        self.playerItem = playerItem
        
        if self.playerItem != nil {
            self.playerItem?.addObserver(self, forKeyPath: PlayerEmptyBufferKey,
                                         options: ([NSKeyValueObservingOptions.new, NSKeyValueObservingOptions.old]),
                                         context: &PlayerItemObserverContext)
            self.playerItem?.addObserver(self, forKeyPath: PlayerKeepUpKey,
                                         options: ([NSKeyValueObservingOptions.new, NSKeyValueObservingOptions.old]),
                                         context: &PlayerItemObserverContext)
            self.playerItem?.addObserver(self, forKeyPath: PlayerStatusKey,
                                         options: ([NSKeyValueObservingOptions.new, NSKeyValueObservingOptions.old]),
                                         context: &PlayerItemObserverContext)
            
            self.playerItem?.addObserver(self, forKeyPath: PlayerLoadedTimeRangesKey,
                                         options: ([NSKeyValueObservingOptions.new, NSKeyValueObservingOptions.old]),
                                         context: &PlayerItemObserverContext)
            
            NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidPlayToEndTime), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.playerItem)
            NotificationCenter.default.addObserver(self, selector: #selector(playerItemFailedToPlayToEndTime), name: NSNotification.Name.AVPlayerItemFailedToPlayToEndTime, object: self.playerItem)
        }
        
        self.player.replaceCurrentItem(with: self.playerItem)
        
        if self.playbackLoops == true {
            self.player.actionAtItemEnd = .none
        } else {
            self.player.actionAtItemEnd = .pause
        }
    }
    
    fileprivate func startIdleCountdown()  {
        if idleTimer != nil {
            idleTimer.invalidate()
        }
        
        idleTimer = Timer.scheduledTimer(timeInterval: delayIdleTimer, target: self, selector: #selector(idleTimerAction), userInfo: nil, repeats: false)
    }
    
    fileprivate func stopIdleCountdown() {
        if idleTimer != nil {
            idleTimer.invalidate()
        }
    }
    
    @objc func idleTimerAction() {
        if playbackState == .playing {
            self.delegate?.playerIdleTimerAfterPlay(self)
        }
    }
    
    // MARK: NSNotifications
    
    @objc open func playerItemDidPlayToEndTime(_ aNotification: Notification) {
        if self.playbackLoops == true || self.playbackFreezesAtEnd == true {
            self.player.seek(to: kCMTimeZero, completionHandler: { completed in
                if completed {
                    self.startIdleCountdown()
                }
            })
        }
        
        if self.playbackLoops == false {
            self.stop()
        }
    }
    
    @objc open func playerItemFailedToPlayToEndTime(_ aNotification: Notification) {
        self.playbackState = .failed
        self.delegate?.playerPlaybackStateDidChange(self)
    }
    
    @objc open func applicationWillResignActive(_ aNotification: Notification) {
        if self.playbackState == .playing {
            //self.pause()
            //stopIdleCountdown()
        }
    }
    
    @objc open func applicationDidEnterBackground(_ aNotification: Notification) {
        if self.playbackState == .playing {
            //self.pause()
            //stopIdleCountdown()
        }
    }
    
    // MARK: KVO
    
    override open func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if (context == &PlayerItemObserverContext) {
            // PlayerStatusKey
            
            if keyPath == PlayerKeepUpKey {
                // PlayerKeepUpKey
                
                if let item = self.playerItem {
                    self.bufferingState = .ready
                    
                    if item.isPlaybackLikelyToKeepUp && self.playbackState == .playing {
                        self.playFromCurrentTime()
                    }
                }
                
                let status = (change?[NSKeyValueChangeKey.newKey] as! NSNumber).intValue as AVPlayerStatus.RawValue
                
                switch (status) {
                case AVPlayerStatus.readyToPlay.rawValue:
                    self.playerView.playerLayer.player = self.player
                    self.playerView.playerLayer.isHidden = false
                case AVPlayerStatus.failed.rawValue:
                    self.playbackState = PlaybackState.failed
                default:
                    break
                }
            } else if keyPath == PlayerEmptyBufferKey {
                // PlayerEmptyBufferKey
                
                if let item = self.playerItem {
                    if item.isPlaybackBufferEmpty {
                        self.bufferingState = .delayed
                    }
                }
                
                let status = (change?[NSKeyValueChangeKey.newKey] as! NSNumber).intValue as AVPlayerStatus.RawValue
                
                switch (status) {
                case AVPlayerStatus.readyToPlay.rawValue:
                    self.playerView.playerLayer.player = self.player
                    self.playerView.playerLayer.isHidden = false
                case AVPlayerStatus.failed.rawValue:
                    self.playbackState = PlaybackState.failed
                default:
                    break
                }
            } else if keyPath == PlayerLoadedTimeRangesKey {
                // PlayerLoadedTimeRangesKey
                
                if let item = self.playerItem {
                    self.bufferingState = .ready
                    
                    let timeRanges = item.loadedTimeRanges
                    if let timeRange: CMTimeRange = timeRanges.first?.timeRangeValue {
                        let bufferedTime = CMTimeGetSeconds(CMTimeAdd(timeRange.start, timeRange.duration))
                        let currentTime = CMTimeGetSeconds(item.currentTime())
                        
                        if (bufferedTime - currentTime) >= self.bufferSize && self.playbackState == .playing {
                            self.playFromCurrentTime()
                        }
                    }
                    
                    self.delegate?.playerLoadedTimeDidChange(self)
                }
            }
            
        } else if (context == &PlayerLayerObserverContext) {
            if self.playerView.playerLayer.isReadyForDisplay {
                self.executeClosureOnMainQueueIfNecessary(withClosure: {
                    self.delegate?.playerReady(self)
                })
            }
        }
    }
}

extension PlayerViewController {
    public func reset() {
        
    }
}

// MARK: - queues

extension PlayerViewController {
    
    internal func executeClosureOnMainQueueIfNecessary(withClosure closure: @escaping () -> Void) {
        if Thread.isMainThread {
            closure()
        } else {
            DispatchQueue.main.async(execute: closure)
        }
    }
    
}

// MARK: - PlayerView

internal class PlayerView: UIView {
    
    var player: AVPlayer! {
        get {
            return (self.layer as! AVPlayerLayer).player
        }
        set {
            (self.layer as! AVPlayerLayer).player = newValue
        }
    }
    
    var playerLayer: AVPlayerLayer! {
        get {
            return self.layer as! AVPlayerLayer
        }
    }
    
    var fillMode: String! {
        get {
            return (self.layer as! AVPlayerLayer).videoGravity.rawValue
        }
        set {
            (self.layer as! AVPlayerLayer).videoGravity = AVLayerVideoGravity.init(rawValue: newValue)
        }
    }
    
    override class var layerClass : AnyClass {
        return AVPlayerLayer.self
    }
    
    // MARK: object lifecycle
    
    convenience init() {
        self.init(frame: CGRect.zero)
        self.playerLayer.backgroundColor = UIColor.black.cgColor
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.playerLayer.backgroundColor = UIColor.black.cgColor
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
