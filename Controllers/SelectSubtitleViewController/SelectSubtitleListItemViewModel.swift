//
//  SelectSubtitleListItemModel.swift
//  CustomPlayerDemo
//
//  Created by Serhiy on 1/30/18.
//  Copyright © 2018 Serhiy. All rights reserved.
//

import Foundation

protocol SelectSubtitleListItemViewModelType {
    var title: String { get }
    var url: URL? { get }
    var isChecked: Bool { get set }
}

class SelectSubtitleListItemViewModel: NSObject, SelectSubtitleListItemViewModelType {
    var title: String
    var url: URL?
    var isChecked: Bool
    
    init(subtitle: SubtitleType) {
        self.title = subtitle.title
        self.url = subtitle.url
        self.isChecked = false
    }
}
