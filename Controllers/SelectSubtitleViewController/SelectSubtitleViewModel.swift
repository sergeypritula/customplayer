//
//  SelectSubtitleViewModel.swift
//  CustomPlayerDemo
//
//  Created by Serhiy on 1/30/18.
//  Copyright © 2018 Serhiy. All rights reserved.
//

import Foundation
import RxSwift

protocol SelectSubtitleViewModelType {
    var subtitles: Variable<[SelectSubtitleListItemViewModelType]> { get }
    func selectSubtitle(subtitle: SelectSubtitleListItemViewModelType)
    func initData(with subtitles: [SubtitleType]?)
}

class SelectSubtitleViewModel: SelectSubtitleViewModelType {
    var subtitles: Variable<[SelectSubtitleListItemViewModelType]> = Variable([SelectSubtitleListItemViewModelType]())
    
    func selectSubtitle(subtitle: SelectSubtitleListItemViewModelType) {
        for i in 0..<subtitles.value.count {
            subtitles.value[i].isChecked = false
        }
        
        if var checkedSubtitle = subtitles.value.first(where: { if let url = $0.url, let subtitleUrl = subtitle.url {
            return url == subtitleUrl && $0.title == subtitle.title
        } else {
            return $0.title == subtitle.title
            }
        }) {
            checkedSubtitle.isChecked = true
        }
    }
    
    func initData(with subtitles: [SubtitleType]?) {
        guard let subtitles = subtitles else {
            return
        }
        
        var viewModels = [SelectSubtitleListItemViewModelType]()
        for subtitle in subtitles {
            viewModels.append(SelectSubtitleListItemViewModel(subtitle: subtitle))
        }
        self.subtitles.value.append(contentsOf: viewModels)
        
        if let title = SavedSubtitleUserDefaultsManager.sharedInstance.getSavedSubtitle() {
            var subtitle = self.subtitles.value.first(where: { $0.title == title })
            subtitle?.isChecked = true
        } else {
            if self.subtitles.value.count > 0 {
                self.subtitles.value[0].isChecked = true
            }
        }
    }
    
}
