//
//  SelectSubtitleViewController.swift
//  CustomPlayerDemo
//
//  Created by Serhiy on 1/30/18.
//  Copyright © 2018 Serhiy. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

protocol SelectSubtitleViewControllerDelegate: class {
    func performSelection(subtitle: SelectSubtitleListItemViewModelType)
}

class SelectSubtitleViewController: UIViewController {
    var viewModel: SelectSubtitleViewModelType! = SelectSubtitleViewModel()
    weak var delegate: SelectSubtitleViewControllerDelegate!
    
    @IBOutlet weak var doneButtonClicked: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    private var disposeBag = DisposeBag()
    
    convenience init(subtitles: [SubtitleType]) {
        self.init()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.makeBindings()
    }
    
    func makeBindings() {
        tableView.register(UINib(nibName: String(describing: SubtitleTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: SubtitleTableViewCell.self))
        
        self.viewModel.subtitles.asObservable()
            .bind(to: tableView.rx.items(cellIdentifier: String(describing: SubtitleTableViewCell.self), cellType: SubtitleTableViewCell.self)) { (row, element, cell) in
                cell.configure(with: element)
            }.disposed(by: disposeBag)
        
        self.tableView.rx.itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                if let item = self?.viewModel.subtitles.value[indexPath.row] {
                    self?.viewModel.selectSubtitle(subtitle: item)
                    self?.tableView.reloadData()
                }
            }).disposed(by: disposeBag)
        
        self.doneButtonClicked.rx.tap.asDriver()
            .drive(onNext: { [weak self] in
                if let checked = self?.viewModel.subtitles.value.first(where: { $0.isChecked }) {
                    self?.delegate.performSelection(subtitle: checked)
                }
                self?.dismiss(animated: true, completion: nil)
            }).disposed(by: disposeBag)
    }
    
}

