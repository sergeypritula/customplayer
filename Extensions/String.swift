//
//  String.swift
//  ShakespeareWorld
//
//  Created by Vitaliy Savchenko on 29.03.16.
//  Copyright © 2016 Onix. All rights reserved.
//

import Foundation
import UIKit

extension String {
    static func stringFromTimeInterval(_ interval:TimeInterval) -> String {
        if !interval.isNaN {
            let timeInterval = NSInteger(interval)
            
            let hours = (timeInterval / 3600)
            let minutes = (timeInterval / 60) % 60
            let seconds = timeInterval % 60
            //        let ms = Int((timeInterval % 1) * 1000)
            
            return String(format: "%0.2d:%0.2d:%0.2d",hours,minutes,seconds)
        }
        return "00:00:00"
    }
    
    func image() -> UIImage {
        return UIImage(named: self)!
    }
    
    
    func url() -> URL? {
        return URL(string: self)
    }
    
    subscript (i: Int) -> Character {
        return self[self.index(self.startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        get {
            let start = self.index(startIndex, offsetBy: r.lowerBound)
            let end = self.index(start, offsetBy: r.upperBound - r.lowerBound)
            return String(self[Range(start ..< end)])
        }
    }
}

